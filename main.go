package main

import (
	"gitlab.com/cameron.vogler/template/loop"
)

func main() {
	if err := loop.Serve(); err != nil {
		panic(err)
	}
}
