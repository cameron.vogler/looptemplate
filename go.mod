module gitlab.com/cameron.vogler/template

go 1.15

require (
	github.com/open-olive/loop-development-kit/ldk/go v0.0.0-20210312191603-357a9e367e82
	github.com/stretchr/testify v1.6.1
)
